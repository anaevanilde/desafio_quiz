//variaveis usadas em mais de uma função
var i=-1;
var pontuacao=0;
var primeira_vez=true;
//Nessa variável estão todas as perguntas e respostas que vocês usarão no desafio
var perguntas = [
    {
        texto: 'O céu não é o limite',
        respostas: [
            { valor: 0, texto: 'Ter preguiça' },
            { valor: 1, texto: 'Quando der certo a gente faz' },
            { valor: 3, texto: 'Vamo pra cima familia' },
            { valor: 2, texto: 'Se for viavel a gente faz' }
        ]
    },
    {
        texto: 'Apaixonados pela missão',
        respostas: [
            { valor: 0, texto: 'Projetos nada mais sao do que numeros' },
            { valor: 1, texto: 'Dinheiro é bom e é isto' },
            { valor: 3, texto: 'Se meu projeto nao ajudar meu cliente entao nao fiz nada' },
            { valor: 2, texto: 'Se eu entender o que esta no codigo entao ta show' }
        ]
    },
    {
        texto: 'Ohana',
        respostas: [
            { valor: 0, texto: 'Ta com problema? Se vira filhao' },
            { valor: 3, texto: 'Voce é meu irmao de batalha, to contigo e nao solto' },
            { valor: 2, texto: 'Voce é da gti, eu tambem entao tudo certo' },
            { valor: 1, texto: 'Fora da Gti nem olho' }
        ]
    },
    {
        texto: 'Integridade',
        respostas: [
            { valor: 0, texto: 'Furar a fila do ru' },
            { valor: 2, texto: 'farol amarelo pra mim é verde' },
            { valor: 1, texto: 'Clonar cartao de credito' },
            { valor: 3, texto: 'Sou integro' }
        ]
    },
    {
        texto: 'Ser capitão da nave',
        respostas: [
            { valor: 3, texto: 'Eu serei o lider que eu quero pra mim' },
            { valor: 0, texto: 'Eu nao sabia, entao nao fiz' },
            { valor: 1, texto: 'Ve la, depois me conta' },
            { valor: 2, texto: 'To so esperando meu estagio aprovar pra sair da GTi' }
        ]
   },
   //questão extra adiconada
   {
        texto: 'Pra mim, um trainee é:',
        respostas: [
            { valor: 0, texto: 'Escória, tem mais é que sofrer mesmo' },
            { valor: 3, texto: 'Praticamente um GTilindo, já faz parte da ohana' },
            { valor: 1, texto: 'É tipo um estagiário, tem que maltratar' },
            { valor: 2, texto: 'Um ser que vive no limbo: não é ohana ainda, mas pode ser' }
        ]
   }
]

function mostrarQuestao() {
     //variável de controle pra ver se tem algo marcado
     var check=0;
     for(var j=0;j<4;j++){
          if(document.getElementsByTagName('input')[j].checked==true){
               check=1;

          }
     }
     /*verifica se tem algo marcado ou ou se é a primeira vez que ele aperta no botão. Se for a primeira vez, ainda vai estar na tela onde o
     botão não submete nada, então só precisa satisfazer uma condição ou outra*/
     if(check==1 || primeira_vez==true){
          //se ele passou pela condição acima então ele já jogou 1 vez, entao atribuo false
          primeira_vez=false;
          //inicia a contabilização das questões
          i++;
          //esta condicional garante o jogo acabe depois que o i seja maior ou igual a cinco (temos questões de 0 até 4)
          if(i>5){
               //verifica a ultima marcação antes de fnalizar
               for(var j=0;j<4;j++){
                    if(document.getElementsByTagName('input')[j].checked==true){
                         pontuacao+=parseInt(document.getElementsByTagName('input')[j].value);
                    }
               }   
               //chama a função responsável por encerrar o jogo            
               finalizarQuiz();
          }
          else{
               //quando o botão for apertado a resposta marcada deve ser contabilizada, essa estrutura checa se há algo marcado e insere na pontuação
               for(var j=0;j<4;j++){
                    if(document.getElementsByTagName('input')[j].checked==true){
                         pontuacao+=parseInt(document.getElementsByTagName('input')[j].value);
                    }
               }
               //muda o texto do botão para "próxima"
               document.getElementById('confirmar').innerHTML= "Próxima";
               /*modifica o style da tabela que mostra os itens a serem marcados, já que no css ela começa com o atributo display=none,
               fazendo com que não apareça na tela*/
               document.getElementById('listaRespostas').style.display="inline";
               //usa o espaço onde estava o título parar escrever o enunciado das questões, para isso ele pega o atributo "texto" da questão va rodada
               document.getElementById('titulo').innerHTML= perguntas[i].texto;


               for(var j=0;j<4;j++){
                    /*para cada opção de resposta (no html é do tipo radio), irá escrever o enunciado da resposta pegando a posição i da pergunta,
                    e varrendo a posições das respostas e pegando o elemento texto*/
                    document.getElementsByClassName('enunciado')[j].innerHTML=perguntas[i].respostas[j].texto;
                    //vai pegar cada elemento input do html e atribuir o valor que está relacionado ao item
                    document.getElementsByTagName('input')[j].value=perguntas[i].respostas[j].valor;

               }
               //zera qualquer opção, se marcada, para garantir que não seja capturada a resposta atual na próxima jogada
               for(var j=0;j<4;j++){
                    if(document.getElementsByTagName('input')[j].checked){
                         document.getElementsByTagName('input')[j].checked=false;
                    }
               }
          }
     }

}

function finalizarQuiz() {
     //devolve o atributo "none" para a tabela de respostas para escondê-las
     document.getElementById('listaRespostas').style.display="none";
     //enuncia o final do jogo e mostra a pontuacao do jogador, usei a parseInt para arredondar o valor e não ficar com muitas casas decimais se for quebrado
     document.getElementById('titulo').innerHTML= "Fim!<br>Sua pontuação foi: "+parseInt((pontuacao*100)/18)+"%";
     //muda o texto do botão parar "jogar novamente"
     document.getElementById('confirmar').innerHTML= "Jogar novamente";
     //zera as váriaveis
     i=-1;
     pontuacao=0;
     //limpa qualquer marcação que esteja feita no rádio para não afetar na próxima rodada
     for(var j=0;j<4;j++){
          if(document.getElementsByTagName('input')[j].checked){
               document.getElementsByTagName('input')[j].checked=false;
          }
     }
     //reseta a variável primeira_vez
     primeira_vez=true;
}
